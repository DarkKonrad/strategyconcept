#pragma once

#include "../Interface/Concepts/RandomAccesIterator.h"
#include "../Interface/Concepts/Comparable.h"

#include <algorithm>
#include <iterator>
#include <iostream>

using std::cout;
using std::endl;

namespace Algorithm
{
	class BubbleSort
	{
	public:

		template<typename Iterator, typename Comparator> requires Interface::RandomAccessIterator<Iterator>
		static void sort(Iterator const& begin, Iterator const& end, Comparator& comparator);

	};

		template<typename Iterator, typename Comparator> requires Interface::RandomAccessIterator<Iterator>
		void BubbleSort::sort(Iterator const& begin, Iterator const& end, Comparator& comparator)
		{
			auto currentIndex = 0;
			auto count = std::distance(begin, end);
		
			for (auto i = 0;i< count;i++)
			{
				auto iter = begin;
				auto limit = count - 1 - i; 
				
				for (auto j =0; j < limit; j++)
				{
					auto onePast = iter + 1;
					if (comparator(*iter, *onePast))
					{
						std::swap(*iter++, *onePast);
					}
					else
					{
						iter++;
					}
		
				}
			}
		}


}