#include "TestPlatform.h"
#include "../Strategy/AlgorithmStrategy.h"

#include <limits>
#include <chrono>
#include <iostream>

long long TestPlatform::Average(std::vector<long long> v)
{
	unsigned long long agregated = 0;
	for (auto it = v.begin(); it != v.end(); it++)
	{
		agregated += *it;
	}

	return agregated / v.size();
}

void TestPlatform::TestAlgorithm(StrategyType strategy, std::string name,bool generateNewValues)
{
	using std::chrono::system_clock;
	using std::cout;
	using std::endl;
	std::vector<long long> measureSec,measureMicro,measureNano;
	this->strategy.setStrategy(strategy);

	for (unsigned short i = 0; i < this->repeat; i++)
	{
		if (generateNewValues == true)
		{
			generateValues();
		}

		system_clock::time_point beforeExecution = system_clock::now();
		this->strategy.Execute(&(this->testObjects));
		system_clock::time_point afterExecution = system_clock::now();
		
		auto seconds = std::chrono::duration_cast<std::chrono::seconds>(afterExecution - beforeExecution).count();
		auto micro = std::chrono::duration_cast<std::chrono::microseconds>(afterExecution - beforeExecution).count();
		auto nano = std::chrono::duration_cast<std::chrono::nanoseconds> (afterExecution - beforeExecution).count();

		measureSec.push_back(seconds);
		measureMicro.push_back(micro);
		measureNano.push_back(nano);

		cout << endl << endl; 
		cout << " Algorithm name: " + name + " Iteration: " << i << " Number of elements " << this->valuesCount << endl << endl;
		cout << " Sorting Time = " << seconds << "[s]" << endl;
		cout << " Sorting Time = " << micro << "[�s]" << endl;
		cout << " Sorting Time = " << nano << "[ns]" << endl;
	}

	cout << endl;
	cout << "Average Sorting time[s]: " << Average(measureSec) << endl;
	cout << "Average Sorting time[�s]: " << Average(measureMicro) << endl;
	cout << "Average Sorting time[ns]: " << Average(measureNano) << endl << endl;


}

void TestPlatform::generateValues()
{
	std::default_random_engine generator;
	std::uniform_real_distribution<double> uniform_dist(std::numeric_limits<double>::min(),
														std::numeric_limits<double>::max());
	
	testObjects.clear();
	auto generateRandomValue = std::bind(uniform_dist, generator);
	for (unsigned i = 0; i < this->valuesCount; i++)
	{
		testObjects.push_back(generateRandomValue());
	}
}

void TestPlatform::TestQuickSort(bool generateNewValues)
{
	TestAlgorithm(StrategyType::QuickSort, "QuickSort");
}

void TestPlatform::TestHeapSort(bool generateNewValues)
{
	TestAlgorithm(StrategyType::HeapSort, "HeapSort");
}

void TestPlatform::TestBubbleSort(bool generateNewValues)
{
	TestAlgorithm(StrategyType::BubbleSort, "BubbleSort");
}

void TestPlatform::TestAll(bool generateNewValues)
{
	TestBubbleSort(generateNewValues);
	TestBuiltInSort(generateNewValues);
	TestHeapSort(generateNewValues);
	TestQuickSort(generateNewValues);
}

void TestPlatform::TestAll_Different_Values_Ammount(bool generateNewValues)
{
	this->SetAmmountOfValues(AmmountOfValues::Low);
	this->TestAll(generateNewValues);
	
	this->SetAmmountOfValues(AmmountOfValues::Medium);
	this->TestAll(generateNewValues);

	this->SetAmmountOfValues(AmmountOfValues::High);
	this->TestAll(generateNewValues);

	this->SetAmmountOfValues(AmmountOfValues::VeryHigh);
	this->TestAll(generateNewValues);
}

void TestPlatform::TestBuiltInSort(bool generateNewValues)
{
	using std::chrono::system_clock;
	using std::cout;
	using std::endl;
	std::vector<long long> measureSec, measureMicro, measureNano;

	for (unsigned short i = 0; i < this->repeat; i++)
	{
		if (generateNewValues == true)
		{
			generateValues();
		}

		system_clock::time_point beforeExecution = system_clock::now();
		std::sort(this->testObjects.begin(), this->testObjects.end(), std::less());
		system_clock::time_point afterExecution = system_clock::now();

		auto seconds = std::chrono::duration_cast<std::chrono::seconds>(afterExecution - beforeExecution).count();
		auto micro = std::chrono::duration_cast<std::chrono::microseconds>(afterExecution - beforeExecution).count();
		auto nano = std::chrono::duration_cast<std::chrono::nanoseconds> (afterExecution - beforeExecution).count();

		measureSec.push_back(seconds);
		measureMicro.push_back(micro);
		measureNano.push_back(nano);

		cout << endl << endl;
		cout << " Algorithm name: std::sort Iteration: " << i << " Number of elements " << this->valuesCount << endl << endl;
		cout << " Sorting Time = " << seconds << "[s]" << endl;
		cout << " Sorting Time = " << micro << "[�s]" << endl;
		cout << " Sorting Time = " << nano << "[ns]" << endl;
	}

	cout << endl;
	cout << "Average Sorting time[s]: " << Average(measureSec) << endl;
	cout << "Average Sorting time[�s]: " << Average(measureMicro) << endl;
	cout << "Average Sorting time[ns]: " << Average(measureNano) << endl << endl;
	
	
}

void TestPlatform::SetAmmountOfValues(AmmountOfValues val)
{
	switch (val)
	{
		case AmmountOfValues::Low: this->valuesCount = 20;
			break;
		case AmmountOfValues::Medium: this->valuesCount = 642;
			break;
		case AmmountOfValues::High: this->valuesCount = 7785;
			break;
		case AmmountOfValues::VeryHigh: this->valuesCount = 25879;
			break;
		case AmmountOfValues::Ultra: this->valuesCount = 125879;
		default:
			break;
	}
}
