#pragma once
#include "../Strategy/AlgorithmStrategy.h"
#include <vector>
#include <random>
 
enum class AmmountOfValues
{
	Low,
	Medium,
	High,
	VeryHigh,
	Ultra
};
class TestPlatform
{

	const unsigned short repeat;
	unsigned int valuesCount;
	AlgorithmStrategy<std::vector<double>, double> strategy;
	std::vector<double> testObjects;
	long long Average(std::vector<long long> v);
public:
	void TestAlgorithm(StrategyType strategy, std::string name, bool generateNewValues = true);
	void TestBuiltInSort(bool generateNewValues = true);
	
	void TestQuickSort(bool generateNewValues = true);
	void TestHeapSort(bool generateNewValues = true);
	void TestBubbleSort(bool generateNewValues = true);
	void TestAll(bool generateNewValues = true);
	void TestAll_Different_Values_Ammount(bool generateNewValues = true);

	void SetAmmountOfValues(AmmountOfValues val);
	void generateValues();
	TestPlatform() : repeat(10), valuesCount(1000) {}

	//Utlility Method
	template<class Iterable_Col> requires Algorithm::Interface::Iterable<Iterable_Col>
	void showCollection(Iterable_Col const& item)
	{
		cout << endl << endl;
		for (auto it = std::begin(item); it != std::end(item); ++it)
		{
			cout << *it << " ";
		}
	}
};

