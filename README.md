**Simple 'Strategy' project for university class.**

Realization of sort interface was made using template and c++ 20 feature - Concepts to provide end user easy to use interface with clear message when
used incorretly. 

Algorithms for use are: 
- HeapSort,
- QuickSort,
- BoubleSort 
- Sort provided by C++ Stadard Library.

Simple test platfrom for speed measurements is also included. 

To compile Visual Studio 2019 is preffered with enbaled features C++ 20 / Experimental.